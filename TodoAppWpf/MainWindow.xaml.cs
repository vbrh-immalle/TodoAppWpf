﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TodoAppWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ObservableCollection<TodoItem> todoItems = new ObservableCollection<TodoItem>()
        {
            new TodoItem() { Beschrijving = "ListBox testen", Done = false },
            new TodoItem() { Beschrijving = "ItemTemplate voor ListBox maken", Done = true},
            new TodoItem() { Beschrijving = "...", Done = false}
        };

        public MainWindow()
        {
            InitializeComponent();

            todoItemsListBox.ItemsSource = todoItems;
        }

        private void DeleteButtonClick(object sender, RoutedEventArgs e)
        {
            //todoItems.Remove((TodoItem)todoItemsListBox.SelectedItem);
            todoItems.Remove(todoItemsListBox.SelectedItem as TodoItem);
        }

        private void AddButtonClick(object sender, RoutedEventArgs e)
        {

        }

        private void ChangeButtonClick(object sender, RoutedEventArgs e)
        {
            TodoItemWindow w = new TodoItemWindow();
            w.DataContext = todoItemsListBox.SelectedItem as TodoItem;
            w.Show();
        }
    }
}
